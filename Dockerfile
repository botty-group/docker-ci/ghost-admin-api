FROM node

ENV DEBIAN_FRONTEND=noninteractive

RUN npm i -g node-fetch && \
    npm i -g jsdom && \
    npm i -g puppeteer && \
    npm i -g @tryghost/admin-api

ENTRYPOINT ["/"]